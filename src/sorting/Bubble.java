package sorting;


import CDAMS.CDRecord;
import CDAMS.CDRecordStorage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Bubble
{
    // CDRecordComparison is used to compare two CDRecords
    private interface CDRecordComparison
    {
        boolean compare(CDRecord left, CDRecord right);
    }

    // SortBy lets the caller sort CDRecords by either Title, Barcode or Author.
    public enum SortBy implements CDRecordComparison
    {
        TITLE ((CDRecord left, CDRecord right) -> (left.getTitle().compareTo(right.getTitle()) > 0)),
        BARCODE ((CDRecord left, CDRecord right) -> (left.getBarcode() > right.getBarcode())),
        AUTHOR ((CDRecord left, CDRecord right) -> (left.getAuthor().compareTo(right.getAuthor()) > 0)),
        SECTION ((CDRecord left, CDRecord right) -> (left.getSection().compareTo(right.getSection()) > 0));

        private CDRecordComparison comparison;

        SortBy(final CDRecordComparison comparison) {
            this.comparison = comparison;
        }

        @Override
        public boolean compare(CDRecord left, CDRecord right) {
            return comparison.compare(left, right);
        }
    }


    /**
     * @param items that need to be sorted
     * @param sortBy either title, barcode, author or section
     */
    public static void sort(CDRecordStorage items, SortBy sortBy)
    {
        boolean isSwapping = true;

        while (isSwapping) {
            isSwapping = false;

            for (int i = 1; i < items.size(); i++)
            {
                CDRecord left = items.select(i-1);
                CDRecord right = items.select(i);

                if (sortBy.compare(left, right))
                {
                    items.update(i, left);
                    items.update(i-1, right);
                    isSwapping = true;
                }
            }
        }
    }

    /**
     * @param items that should be sorted
     * @param sortBy either title, barcode, author or section
     */
    public static void sort(List<CDRecord> items, SortBy sortBy)
    {
        boolean isSwapping = true;

        while (isSwapping) {
            isSwapping = false;

            for (int i = 1; i < items.size(); i++)
            {
                CDRecord left = items.get(i-1);
                CDRecord right = items.get(i);

                if (sortBy.compare(left, right))
                {
                    items.set(i, left);
                    items.set(i-1, right);
                    isSwapping = true;
                }
            }
        }
    }

}

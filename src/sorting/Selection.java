package sorting;

import CDAMS.CDRecord;

import java.util.ArrayList;
import java.util.List;

public class Selection
{
    public static void main(String[] args)
    {
        List<CDRecord> records = new ArrayList<>();

        for (int i = 0; i < 20; i++)
        {
            int randBarcode = (int)(Math.random() * 100);
            records.add(new CDRecord(randBarcode));
        }

        System.out.println("Before sort " + records.toString());

        sort(records);

        System.out.println("After sort " + records.toString());

    }

    public static void sort(List<CDRecord> records)
    {
        for (int i = 1; i < records.size() -1; i++)
        {
            int smallestIndex = i;
            for (int j = i + 1; j < records.size(); j++)
            {
                if (records.get(j).getBarcode() < records.get(smallestIndex).getBarcode())
                    smallestIndex = j;

                CDRecord smallestRecord = records.get(smallestIndex);
                CDRecord indexRecord = records.get(i);
                records.set(i, smallestRecord);
                records.set(smallestIndex, indexRecord);
            }
        }
    }
}

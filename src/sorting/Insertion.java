package sorting;

import CDAMS.CDRecord;

import java.util.ArrayList;
import java.util.List;

public class Insertion
{
    public static void main(String[] args)
    {
        List<CDRecord> records = new ArrayList<>();

        for (int i = 0; i < 20; i++)
        {
            int randBarcode = (int)(Math.random() * 100);
            records.add(new CDRecord(randBarcode));
        }

        System.out.println("Before sort " + records.toString());

        sort(records);

        System.out.println("After sort " + records.toString());

    }
    public static void sort(List<CDRecord> records)
    {
        for (int i = 1; i < records.size(); i++)
        {
           CDRecord indexRecord = records.get(i);
           int previousIndex = i - 1;
           while (previousIndex >= 0 && records.get(previousIndex).getBarcode() > indexRecord.getBarcode())
           {
               CDRecord previousRecord = records.get(previousIndex);
               records.set(previousIndex + 1, previousRecord);
               previousIndex--;
           }

           records.set(previousIndex + 1, indexRecord);
        }
    }
}

package util;

import java.awt.*;

public class Component {

    /**
     * @param contentPane to add the component to
     * @param component the component to add
     * @param fill of the component
     * @param gridX of the component on the content pane
     * @param gridY of the component on the content pane
     * @param gridWidth of the component on the content pane
     * @param gridHeight of the component on the content pane
     * @param weightX of the component on the content pane
     * @param weightY of the component on the content pane
     * @param <C> The type of component
     */
    public static <C extends java.awt.Component> void add(
            Container contentPane,
            C component,
            int fill, int gridX, int gridY,
            int gridWidth, int gridHeight,
            float weightX, float weightY
    )
    {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = fill;
        constraints.gridx = gridX;
        constraints.gridy = gridY;
        constraints.gridwidth = gridWidth;
        constraints.gridheight = gridHeight;
        constraints.weightx = weightX;
        constraints.weighty = weightY;

        contentPane.add(component, constraints);
    }

    /**
     * @param contentPane to add the component to
     * @param component the component to add
     * @param fill of the component
     * @param gridX of the component on the content pane
     * @param gridY of the component on the content pane
     * @param gridWidth of the component on the content pane
     * @param gridHeight of the component on the content pane
     * @param weightX of the component on the content pane
     * @param weightY of the component on the content pane
     * @param <C> The type of component
     * @param insets or "padding" of the component on the content pane
     * @param anchor of the component on the content pane
     */
    public static <C extends java.awt.Component> void add(
            Container contentPane,
            C component,
            int fill, int gridX, int gridY,
            int gridWidth, int gridHeight,
            float weightX, float weightY,
            Insets insets, int anchor
    )
    {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = fill;
        constraints.gridx = gridX;
        constraints.gridy = gridY;
        constraints.gridwidth = gridWidth;
        constraints.gridheight = gridHeight;
        constraints.weightx = weightX;
        constraints.weighty = weightY;
        constraints.insets = insets;
        constraints.anchor = anchor;

        contentPane.add(component, constraints);
    }
}

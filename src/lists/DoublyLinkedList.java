package lists;

import java.util.ArrayList;
import java.util.List;

public class DoublyLinkedList
{
    Node head;

    public DoublyLinkedList(Object data)
    {
        this.head = new Node(data);
    }

    public DoublyLinkedList() {}

    public static class Node
    {
        Object data;
        Node next, prev;

        public Node(Object data)
        {
           this.data = data;
        }

        public Object getData()
        {
            return this.data;
        }

        public void setData(Object data)
        {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public Node getPrev() {
            return prev;
        }
    }

    /**
     * @param node to look into
     * @return the next node (if any)
     */
    private Node traverseList(Node node)
    {
        return node.next != null ? traverseList(node.next) : node;
    }

    /**
     * @param data append an object to the list
     */
    public void append(Object data)
    {
        Node new_node = new Node(data);

        if (this.head == null)
        {
            this.head = new_node;
            return;
        }

        Node last = this.head;

        last = traverseList(last);

        last.next = new_node;
        new_node.prev = last;
    }

    /**
     * @param data to be prepended to the list
     */
    public void prepend(Object data)
    {
        Node new_node = new Node(data);

        if (this.head == null)
        {
            this.head = new_node;
            return;
        }

        new_node.next = this.head;
        new_node.prev = null;
        if (this.head != null)
            this.head.prev = new_node;

        this.head = new_node;
    }

    /**
     * @param data to be found
     * @return the node of the data, if not found then returns null
     */
    public Node find(Object data)
    {
        Node node = this.head;

        while(node != null)
        {
           if (node.data == data)
               return node;

           node = node.next;
        }

        return null;
    }

    /**
     * @param data of the node to be found
     * @return the deleted node or null if data cannot be found in the list
     */
    public Node delete(Object data)
    {
        Node node = this.find(data);

        if (node == null || this.head == null)
            return null;

        if (node.prev == null)
            this.head = node.next;

        if (node.next != null)
            node.next.prev = node.prev;

        if (node.prev != null)
            node.prev.next = node.next;

        return node;
    }

    /**
     * @param src the object "data" should be placed after
     * @param data the object that should be placed after "src"
     */
    public void insertAfter(Object src, Object data)
    {
        Node src_node = this.find(src);

        if (src_node.next == null)
        {
           this.append(data);
           return;
        }

        Node new_node = new Node(data);
        
        new_node.next = src_node.next;
        src_node.next = new_node;
        new_node.prev = src_node;
        new_node.next.prev = new_node;
    }

    public String toString()
    {
        String str = "Doubly Linked List: \n";
        Node node = this.head;

        while(node != null)
        {
            str += node.data;
            if (node.next != null)
                str += "\n";
            node = node.next;
        }

        return str;
    }

    public String toStringReverse()
    {
        Node last = this.head;
        last = this.traverseList(last);

        String str = "Doubly Linked List: ";

        while (last != null)
        {
            str += last.data;
            if (last.prev != null)
                str += " <- ";
            last = last.prev;
        }

        return str;
    }
}
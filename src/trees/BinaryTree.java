package trees;

import java.util.ArrayList;
import java.util.List;

public class BinaryTree
{
    Node root;
    public BinaryTree()
    {
    }

    public BinaryTree(List<Node> nodes)
    {
        for (Node node : nodes)
            this.insert(node);
    }

    public static class Node
    {
        int key;
        Object data;
        Node left, right;
        public Node(int key, Object data)
        {
            this.key = key;
            this.data = data;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public int getKey() {
            return key;
        }

        public String toString() {
            return Integer.toString(this.key) + " = " + this.data.toString();
        }
    }

    public void insert(Node node)
    {
        if (this.root == null)
            this.root = node;
        else
            insert(this.root, node);
    }

    private void insert(Node focusNode, Node node)
    {
        if (node.key < focusNode.key)
            if (focusNode.left == null)
                focusNode.left = node;
            else
                insert(focusNode.left, node);

        else if (node.key > focusNode.key)
            if (focusNode.right == null)
                focusNode.right = node;
            else
                insert(focusNode.right, node);
    }


    /**
     * @return An array list of nodes that are sorted "in order"
     */
    public ArrayList<Node> traverseInOrder()
    {
        return traverseInOrder(this.root);
    }

    private ArrayList<Node> traverseInOrder(Node focusNode)
    {
        ArrayList<Node> nodeList = new ArrayList<Node>();

        if (focusNode != null)
        {
            nodeList.addAll(traverseInOrder(focusNode.left));
            nodeList.add(focusNode);
            nodeList.addAll(traverseInOrder(focusNode.right));
        }

        return nodeList;
    }

    /**
     * @return An array list of nodes that are sorted in "pre order"
     */
    public ArrayList<Node> traversePreOrder()
    {
       return traversePreOrder(this.root);
    }

    private ArrayList<Node> traversePreOrder(Node focusNode)
    {
        ArrayList<Node> nodeList = new ArrayList<Node>();

        if (focusNode != null)
        {
            nodeList.add(focusNode);
            nodeList.addAll(traverseInOrder(focusNode.left));
            nodeList.addAll(traverseInOrder(focusNode.right));
        }

        return nodeList;
    }

    /**
     * @return An array list of nodes that are sorted in "post order"
     */
    public ArrayList<Node> traversePostOrder()
    {
        return traversePostOrder(this.root);
    }

    private ArrayList<Node> traversePostOrder(Node focusNode)
    {
        ArrayList<Node> nodeList = new ArrayList<Node>();

        if (focusNode != null)
        {
            nodeList.addAll(traverseInOrder(focusNode.left));
            nodeList.addAll(traverseInOrder(focusNode.right));
            nodeList.add(focusNode);
        }

        return nodeList;
    }


    /**
     * @param key of the node
     * @return the node associated with the key, if no node was found then null is returned
     */
    public Node find(int key)
    {
        return find(this.root, key);
    }

    private Node find(Node focusNode, int key)
    {
       if (focusNode.key == key)
           return focusNode;
       else if (key < focusNode.key)
           return find(focusNode.left, key);
       else if (key > focusNode.key)
           return find(focusNode.right, key);
       else
           return null;
    }
}

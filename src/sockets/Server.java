package sockets;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements MessageSender {

    public static void main(String[] args) {
        Server server = new Server(20000, new MessageListener() {
            @Override
            public void message(String msg, MessageSender sender) {
                try
                {
                    if (msg.contains("SEND"))
                        sender.send(msg);
                } catch(Exception e)
                {
                    System.out.println("Thread cannot sleep: " + e.getMessage());
                }
            }
        });
    }

    ServerSocket connectionListener;
    MessageListener messageListener;
    ArrayList<ServerClient> clients;


    /**
     * @param port the server should bind to on the system
     * @param messageListener message listener
     */
    public Server(int port, MessageListener messageListener)
    {
        this.messageListener = messageListener;
        MessageSender sender = this;
        this.clients = new ArrayList<>();

        try
        {
            this.connectionListener = new ServerSocket(port);
            System.out.println("Server is listening...");

            boolean running = true;
            while (running)
            {
                ServerClient client = new ServerClient(this.connectionListener.accept());
                this.clients.add(client);
                System.out.println("Client " + client.connection.getInetAddress() + " has connected");

                (new Thread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Started client message listener");

                        try
                        {
                            String readLine;
                            while (((readLine = client.inputStream.readLine())) != null && client.connection.isConnected())
                            {
                                messageListener.message(readLine, sender);
                            }
                        } catch (Exception e)
                        {
                            System.err.println("Failed to read line: " + e.getMessage());
                        }
                    }
                })).start();

            }
        }
        catch (Exception e)
        {
            System.err.println("There was an error: " + e.getMessage());
        }
    }

    /**
     * @param msg message to be sent to all connected clients
     */
    @Override
    public void send(String msg) {
        for (ServerClient client : clients)
        {
            if (client.connection.isClosed())
            {
                clients.remove(client);
            } else {
                client.outputStream.println(msg);
            }
        }
    }

    class ServerClient {
        Socket connection;
        PrintWriter outputStream;
        BufferedReader inputStream;

        /**
         * @param connection socket to send data to
         */
        public ServerClient(Socket connection)
        {
            this.connection = connection;
            try
            {
                outputStream = new PrintWriter(connection.getOutputStream(), true);
                inputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }
            catch (Exception e)
            {
                System.err.println("There was an error: " + e.getMessage());
            }
        }
    }
}

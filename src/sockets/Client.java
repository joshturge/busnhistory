package sockets;

import java.io.*;
import java.net.Socket;

public class Client implements MessageSender {

    Socket connection;
    PrintWriter outputStream;
    BufferedReader inputStream;

    MessageListener messageListener;
    Thread messageListenerThread;

    public static void main(String[] args) {
        Client client = new Client("localhost:20000", new MessageListener() {
            @Override
            public void message(String msg, MessageSender sender) {
                System.out.println(msg);
                sender.send("Message received");
            }
        });
    }

    /**
     * @param address of the server
     * @param messageListener message listener
     */
    public Client(String address, MessageListener messageListener)
    {
        this.messageListener = messageListener;
        MessageSender sender = this;

        String host = "localhost";
        int port = 20000;

        if (address.contains(":"))
        {
            String[] hostAndPort = address.split(":");
            host = hostAndPort[0];
            port = Integer.parseInt(hostAndPort[1]);
        } else {
            host = address;
        }

        System.out.println("Opening a connection on: " + host + ":" + port);

        try
        {
            this.connection = new Socket(host, port);

            this.outputStream = new PrintWriter(this.connection.getOutputStream(), true);
            this.inputStream = new BufferedReader(new InputStreamReader(this.connection.getInputStream()));

            this.messageListenerThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        for(String line = inputStream.readLine(); line != null; line = inputStream.readLine())
                        {
                            messageListener.message(line, sender);
                        }
                    }
                    catch (Exception e)
                    {
                        System.err.println("An error has occurred: " + e.getMessage());
                    }

                }
            });

            messageListenerThread.start();


        } catch (Exception e)
        {
            System.err.println("An error has occurred: " + e.getMessage());
        }
    }


    /**
     * @return whether the client is connected to a socket server
     */
    public boolean isConnected()
    {
        return this.connection != null && outputStream != null && inputStream != null;
    }

    /**
     * Disconnects the client from the server
     */
    public void disconnect()
    {
        if(this.isConnected())
        {
            try
            {
                this.inputStream = null;
                this.outputStream = null;
                this.connection.close();
            }
            catch (Exception e)
            {
                System.err.println("An error has occurred: " + e.getMessage());
            }
        }
    }

    /**
     * @param msg the message to send to the server
     */
    @Override
    public void send(String msg) {
        if(this.isConnected())
        {
            this.outputStream.println(msg);
        }
    }
}

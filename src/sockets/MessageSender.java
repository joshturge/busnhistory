package sockets;

public interface MessageSender {
    void send(String msg);
}

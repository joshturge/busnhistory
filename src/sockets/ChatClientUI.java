package sockets;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ChatClientUI {

    private JFrame window;
    Client client;

    JList chatHistory;
    ArrayList<String> chatHistoryData;

    public ChatClientUI() {
       this.window = new JFrame("Chat Client");
       this.window.setMinimumSize(new Dimension(400, 400));
       this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

       Container content = this.window.getContentPane();
       content.setLayout(new GridBagLayout());

       this.chatHistoryData = new ArrayList<>();


       // init UI elements
        createUI();

       this.window.pack();
       this.window.setVisible(true);
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ChatClientUI client = new ChatClientUI();

            }
        });
    }

    private void createUI()
    {
        JTextField serverAddress = new JTextField();
        addComponent(serverAddress, 0, 0, 2, 1, 0f, 0f);

        JButton connect = new JButton("Connect");
        addComponent(connect, 0, 1, 2, 1, 1f, 0f);
        connect.addActionListener(e -> {
            System.out.println("Connect was pressed with: " + serverAddress.getText());

            this.client = new Client(serverAddress.getText(), new MessageListener() {
                @Override
                public void message(String msg, MessageSender sender) {
                    chatHistoryData.add(msg);
                    chatHistory.setListData(chatHistoryData.toArray());
                }
            });
        });

        JList chatHistory = new JList();
        addComponent(chatHistory, 0, 2, 2, 1, 1f, 1f);

        JTextField chatBox = new JTextField();
        addComponent(chatBox, 0, 3, 1, 1, 1f, 0f);

        JButton send = new JButton("Send");
        addComponent(send, 1, 3, 1,1, 0f, 0f);
        send.addActionListener(e -> {
            System.out.println("Send was pressed with: " + chatBox.getText());
            if (this.client != null)
            {
                this.client.send(chatBox.getText());
            }
            chatBox.setText("");
        });
    }

    private <C extends Component> C addComponent(C component, int gridX, int gridY, int gridWidth, int gridHeight,
                                                 float weightX, float weightY)
    {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = gridX;
        constraints.gridy = gridY;
        constraints.gridwidth = gridWidth;
        constraints.gridheight= gridHeight;
        constraints.weightx = weightX;
        constraints.weighty = weightY;

        this.window.getContentPane().add(component, constraints);

        return component;

    }
}

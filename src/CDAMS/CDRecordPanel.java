package CDAMS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CDRecordPanel extends JPanel {
    private JLabel titleLabel, authorLabel, sectionLabel,
                    xLabel, yLabel, barcodeLabel,
                    descriptionLabel, onLoanLabel;
    private JTextField titleText, authorText,
                        sectionText, xText, yText,
                        barcodeText, descriptionText;

    private JCheckBox onLoanCheckBox;

    private JButton newItemButton, saveUpdateButton;

    private CDRecordStorage records;
    private int selectedIndex;

    private boolean isNewRecord = true;

    public CDRecordPanel(CDRecordStorage records)
    {
        this.records = records;

        createUI();
        createActionListeners();
    }

    private void createUI()
    {
        this.setLayout(new GridBagLayout());

        this.titleLabel = new JLabel("Title:");
        util.Component.add(this,
                this.titleLabel,
                GridBagConstraints.BOTH,
                0, 0, 1, 1, 0, 0
        );

        this.titleText = new JTextField();
        util.Component.add(this,
                this.titleText,
                GridBagConstraints.BOTH,
                1, 0, 1, 1, 40, 0
        );

        this.authorLabel = new JLabel("Author:");
        util.Component.add(this,
                this.authorLabel,
                GridBagConstraints.BOTH,
                0, 1, 1, 1, 0, 0
        );

        this.authorText = new JTextField();
        util.Component.add(this,
                this.authorText,
                GridBagConstraints.BOTH,
                1, 1, 1, 1, 40, 0
        );

        this.sectionLabel = new JLabel("Section:");
        util.Component.add(this,
                this.sectionLabel,
                GridBagConstraints.BOTH,
                0, 2, 1, 1, 0, 0
        );

        this.sectionText = new JTextField();
        util.Component.add(this,
                this.sectionText,
                GridBagConstraints.BOTH,
                1, 2, 1, 1, 40, 0
        );

        this.xLabel = new JLabel("X:");
        util.Component.add(this,
                this.xLabel,
                GridBagConstraints.BOTH,
                0, 3, 1, 1, 0, 0
        );

        this.xText = new JTextField();
        util.Component.add(this,
                this.xText,
                GridBagConstraints.BOTH,
                1, 3, 1, 1, 40, 0
        );

        this.yLabel = new JLabel("Y:");
        util.Component.add(this,
                this.yLabel,
                GridBagConstraints.BOTH,
                0, 4, 1, 1, 0, 0
        );

        this.yText = new JTextField();
        util.Component.add(this,
                this.yText,
                GridBagConstraints.BOTH,
                1, 4, 1, 1, 40, 0
        );

        this.barcodeLabel = new JLabel("Barcode:");
        util.Component.add(this,
                this.barcodeLabel,
                GridBagConstraints.BOTH,
                0, 5, 1, 1, 0, 0
        );

        this.barcodeText = new JTextField();
        util.Component.add(this,
                this.barcodeText,
                GridBagConstraints.BOTH,
                1, 5, 1, 1, 40, 0
        );

        this.descriptionLabel = new JLabel("Description:");
        util.Component.add(this,
                this.descriptionLabel,
                GridBagConstraints.BOTH,
                0, 6, 1, 1, 0, 0
        );

        this.descriptionText = new JTextField();
        util.Component.add(this,
                this.descriptionText,
                GridBagConstraints.BOTH,
                1, 6, 1, 1, 40, 0
        );

        this.onLoanLabel = new JLabel("On Loan:");
        util.Component.add(this,
                this.onLoanLabel,
                GridBagConstraints.BOTH,
                0, 7, 1, 1, 0, 0
        );

        this.onLoanCheckBox = new JCheckBox();
        util.Component.add(this,
                this.onLoanCheckBox,
                GridBagConstraints.BOTH,
                1, 7, 1, 1, 0, 0
        );

        this.newItemButton = new JButton("New Item");
        util.Component.add(this,
                this.newItemButton,
                GridBagConstraints.VERTICAL,
                0, 8, 1, 1, 0.0f, 0.0f
        );

        this.saveUpdateButton = new JButton("Save / Update");
        util.Component.add(this,
                this.saveUpdateButton,
                GridBagConstraints.VERTICAL,
                1, 8, 1, 1, 0.0f, 0.0f,
                new Insets(0, 0, 0, 0),
                GridBagConstraints.EAST
        );
    }

    private void createActionListeners()
    {
        this.saveUpdateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().isEmpty() || authorText.getText().isEmpty() ||
                        sectionText.getText().isEmpty() || xText.getText().isEmpty() ||
                        yText.getText().isEmpty() || barcodeText.getText().isEmpty() ||
                        descriptionText.getText().isEmpty())
                    return;

                CDRecord record = new CDRecord(titleText.getText(),
                        authorText.getText(),
                        sectionText.getText(),
                        Integer.parseInt(xText.getText()),
                        Integer.parseInt(yText.getText()),
                        Integer.parseInt(barcodeText.getText()),
                        descriptionText.getText(),
                        onLoanCheckBox.isSelected()
                );

                if (!isNewRecord)
                    records.update(selectedIndex, record);
                else
                    records.create(record);

                isNewRecord = true;

                clearCDRecord();
            }
        });

        this.newItemButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                clearCDRecord();
                isNewRecord = true;
            }
        });
    }

    public void setCDRecord(int index)
    {
        this.selectedIndex = index;
        CDRecord record = this.records.select(this.selectedIndex);

        this.titleText.setText(record.getTitle());
        this.authorText.setText(record.getAuthor());
        this.sectionText.setText(record.getSection());
        this.xText.setText(Integer.toString(record.getX()));
        this.yText.setText(Integer.toString(record.getY()));
        this.barcodeText.setText(Integer.toString(record.getBarcode()));
        this.descriptionText.setText(record.getDescription());
        this.onLoanCheckBox.setSelected(record.isOnLoan());

        this.isNewRecord = false;
    }

    public void clearCDRecord()
    {
        this.titleText.setText("");
        this.authorText.setText("");
        this.sectionText.setText("");
        this.xText.setText("");
        this.yText.setText("");
        this.barcodeText.setText("");
        this.descriptionText.setText("");
        this.onLoanCheckBox.setSelected(false);
    }

}

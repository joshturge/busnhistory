package CDAMS;

import java.util.Arrays;
import java.util.List;

public class CDRecord
{
    String title, author, section, description;
    int barcode;
    int x, y;

    public boolean isOnLoan() {
        return onLoan;
    }

    public void setOnLoan(boolean onLoan) {
        this.onLoan = onLoan;
    }

    boolean onLoan;

    public CDRecord(int barcode)
    {
        this.barcode = barcode;
    }
    public CDRecord(String title, String author, String section, int x, int y, int barcode, String description, boolean onLoan)
    {
        this.title = title;
        this.author = author;
        this.section = section;
        this.x = x;
        this.y = y;
        this.barcode = barcode;
        this.description = description;
        this.onLoan = onLoan;
    }

    public int getBarcode() {
        return barcode;
    }

    public static List<CDRecord> getTestData()
    {
        CDRecord[] records = new CDRecord[]
                {
                    new CDRecord("Something", "Joe", "somef", 34, 34, 354634, "Something", true),
                        new CDRecord("Something1", "Joe", "somef", 34, 34, 354634, "Something", true),
                        new CDRecord("Something2", "Joe", "somef", 34, 34, 354634, "Something", true),
                        new CDRecord("Something3", "Joe", "somef", 34, 34, 354634, "Something", true),
                        new CDRecord("Something4", "Joe", "somef", 34, 34, 354634, "Something", true),
                };

        return Arrays.asList(records);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBarcode(int barcode) {
        this.barcode = barcode;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "CDRecord{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", section='" + section + '\'' +
                ", description='" + description + '\'' +
                ", barcode=" + barcode +
                ", x=" + x +
                ", y=" + y +
                ", onLoan=" + onLoan +
                '}';
    }
}

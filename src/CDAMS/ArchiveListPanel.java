package CDAMS;

import sorting.Bubble;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ArchiveListPanel extends JPanel {
    private JLabel searchLabel;
    private JTextField searchText;
    private JButton searchButton;

    private JLabel titleLabel, sortLabel;
    private JTable cdRecordsTable;
    private JScrollPane cdRecordScrollPane;
    private JButton sortByTitleButton, sortByBarcodeButton, sortByAuthorButton;

    CDRecordStorage records;
    CDRecordPanel cdRecordPanel;
    ProcessLogPanel processLogPanel;

    int selectedCDRecordIndex = -1;

    public ArchiveListPanel(CDRecordStorage records, CDRecordPanel cdRecordPanel, ProcessLogPanel processLogPanel)
    {
        this.records = records;
        this.cdRecordPanel = cdRecordPanel;
        this.processLogPanel = processLogPanel;

        createUI();
        createActionListeners();

        this.cdRecordsTable.setModel(this.records.getTableModel());
        this.cdRecordsTable.setFillsViewportHeight(true);
    }

    private void createUI()
    {
        this.setLayout(new GridBagLayout());

        this.searchLabel = new JLabel("Search:");
        util.Component.add(this,
                this.searchLabel,
                GridBagConstraints.HORIZONTAL,
                0, 0, 1, 1, 0, 0
        );

        this.searchText = new JTextField();
        util.Component.add(this,
                this.searchText,
                GridBagConstraints.HORIZONTAL,
                1, 0, 1, 1, 400, 0,
                new Insets(0, 10, 0, 10),
                GridBagConstraints.CENTER
        );

        this.searchButton = new JButton("Search");
        util.Component.add(this,
                this.searchButton,
                GridBagConstraints.HORIZONTAL,
                2, 0, 1, 1, 20, 0);

        this.titleLabel = new JLabel("Archive CDs");
        util.Component.add(this,
                this.titleLabel,
                GridBagConstraints.BOTH,
                0, 1, 4, 1, 100, 0
        );

        this.cdRecordsTable = new JTable();
        this.cdRecordScrollPane = new JScrollPane(this.cdRecordsTable);
        util.Component.add(this,
                this.cdRecordScrollPane,
                GridBagConstraints.BOTH,
                0, 2, 4, 1, 100, 10
        );

        this.sortLabel = new JLabel("Sort:");
        util.Component.add(this,
                this.sortLabel,
                GridBagConstraints.BOTH,
                0, 3, 1, 1, 0, 0
        );

        this.sortByTitleButton = new JButton("By Title");
        util.Component.add(this,
                this.sortByTitleButton,
                GridBagConstraints.HORIZONTAL,
                1, 3, 1, 1, 0, 0,
                new Insets(0, 50, 0, 50),
                GridBagConstraints.WEST
        );

        this.sortByBarcodeButton = new JButton("By Barcode");
        util.Component.add(this,
                this.sortByBarcodeButton,
                GridBagConstraints.HORIZONTAL,
                2, 3, 1, 1, 0, 0,
                new Insets(0, 50, 0, 50),
                GridBagConstraints.WEST
        );

        this.sortByAuthorButton = new JButton("By Author");
        util.Component.add(this,
                this.sortByAuthorButton,
                GridBagConstraints.HORIZONTAL,
                3, 3, 1, 1, 0, 0,
                new Insets(0, 50, 0, 0),
                GridBagConstraints.WEST
        );
    }

    private void createActionListeners()
    {
        this.searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CDRecord record;

                for (int i = 0; i < records.size(); i++)
                {
                    record = records.select(i);
                    if (record.getTitle().equalsIgnoreCase(searchText.getText()))
                    {
                        selectedCDRecordIndex = -1;
                        cdRecordsTable.clearSelection();
                        cdRecordsTable.addRowSelectionInterval(i, i);

                        cdRecordPanel.setCDRecord(i);
                        break;
                    }
                }
            }
        });

        this.cdRecordsTable.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                selectedCDRecordIndex = cdRecordsTable.getSelectedRow();

                cdRecordPanel.setCDRecord(selectedCDRecordIndex);
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });

        this.sortByTitleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Bubble.sort(records, Bubble.SortBy.TITLE);
                processLogPanel.fillTree(records.getList());
            }
        });

        this.sortByBarcodeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Bubble.sort(records, Bubble.SortBy.BARCODE);
            }
        });

        this.sortByAuthorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Bubble.sort(records, Bubble.SortBy.AUTHOR);
            }
        });
    }

    /**
     * Please note that if no cd record is selected then the index is -1.
     * @return the currently selected cd records index in storage
     */
    public CDRecord getSelectedCDRecord()
    {
        return this.records.select(this.selectedCDRecordIndex);
    }
}

package CDAMS;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.*;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CDRecordStorage {

    private List<CDRecord> records;
    private CDRecordTableModel tableModel;

    public CDRecordStorage()
    {
        this.records = new ArrayList<>();
    }

    public CDRecordStorage(String filename)
    {
        this.records = new ArrayList<>();
        this.tableModel = new CDRecordTableModel(this.records);
        this.loadList(filename);
    }

    public void loadList(String filename)
    {
        try
        {
            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);

            String line;
            while ((line = br.readLine()) != null)
            {
                String[] dataColumns = line.split(";");
                CDRecord record = new CDRecord(
                        dataColumns[1],
                        dataColumns[2],
                        dataColumns[3],
                        Integer.parseInt(dataColumns[4]),
                        Integer.parseInt(dataColumns[5]),
                        Integer.parseInt(dataColumns[6]),
                        dataColumns[7],
                        dataColumns[8] == "Yes"
                );

                this.records.add(record);
            }

            br.close();
        } catch (Exception e)
        {
            System.err.println("Failed to load file: "+e.getMessage());
        }
    }

    public void create(CDRecord record)
    {
        this.records.add(record);
        this.tableModel.fireTableDataChanged();
    }

    public CDRecord select(int index)
    {
        return this.records.get(index);
    }

    public void update(int index, CDRecord record)
    {
        this.records.set(index, record);
        this.tableModel.fireTableDataChanged();
    }

    public void delete(int index)
    {
        this.records.remove(index);
        this.tableModel.fireTableDataChanged();
    }

    public int size()
    {
        return this.records.size();
    }

    public void saveList(String filename)
    {
        try
        {
            FileWriter fw = new FileWriter(filename);
            BufferedWriter bf = new BufferedWriter(fw);

            CDRecord record;

            for (int i = 0; i < this.records.size(); i++)
            {
                record = this.records.get(i);
                bf.write(i+1 + ";" + record.getTitle() + ";" + record.getAuthor() + ";" + record.getSection() + ";" + record.getX() + ";" + record.getY() + ";" + record.getBarcode() + ";" + record.getDescription() + ";" + (record.isOnLoan() ? "Yes" : "No"));
                bf.newLine();
            }

            bf.close();
        } catch (Exception e)
        {
            System.err.println("Could not write to file: " + e.getMessage());
        }
    }

    public void saveCSV(String filename)
    {
        try
        {
            BufferedWriter bw = Files.newBufferedWriter(Paths.get(filename)) ;

            CSVPrinter printer = new CSVPrinter(bw, CSVFormat.DEFAULT.withHeader("Title", "Author", "Section", "X", "Y", "Barcode", "Description", "On Loan"));

            for (CDRecord record : this.records)
            {
                printer.printRecord(record.getTitle(), record.getAuthor(), record.getSection(), record.getX(), record.getY(), record.getBarcode(), record.getDescription(), record.isOnLoan());
            }

            printer.flush();

        } catch(Exception e)
        {
            System.err.println("Unable to save to csv: " + e.getMessage());
        }
    }

    public List<CDRecord> getList()
    {
        return this.records;
    }

    @Override
    public String toString() {
        return this.records.toString();
    }

    public CDRecordTableModel getTableModel()
    {
        return this.tableModel;
    }
}

package CDAMS;

import sockets.Client;
import sockets.MessageListener;
import sockets.MessageSender;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionRequestPanel extends JPanel {
    private JLabel actionRequestLabel, sortSectionLabel;

    private JButton retrieveButton, removeButton, returnButton, addCollectionButton, randColSortButton, mostlySortedSortButton, reverseOrderSortButton, exitButton;

    private JTextField sortSectionText;

    private ArchiveListPanel archiveListPanel;
    private ProcessLogPanel processLogPanel;

    private CDRecordStorage records;

    private Client client;

    public ActionRequestPanel(CDRecordStorage records, ArchiveListPanel archiveListPanel, ProcessLogPanel processLogPanel)
    {
        this.archiveListPanel = archiveListPanel;
        this.records = records;
        this.processLogPanel = processLogPanel;

        this.client = new Client("localhost:20000", new MessageListener() {
            @Override
            public void message(String msg, MessageSender sender) {
                System.out.println(msg);
                processLogPanel.logProcess(msg);
            }
        });

        createUI();
        createActionListeners();
    }

    private void createUI()
    {
        this.setLayout(new GridBagLayout());

        this.actionRequestLabel = new JLabel("Automation Action Request for the item above:");
        util.Component.add(this,
                this.actionRequestLabel,
                GridBagConstraints.HORIZONTAL,
                0, 0, 1, 1, 0.5f, 0.5f
        );

        this.retrieveButton = new JButton("Retrieve");
        util.Component.add(this,
                this.retrieveButton,
                GridBagConstraints.HORIZONTAL,
                0, 1, 1, 1, 0.5f, 0.5f
        );

        this.removeButton = new JButton("Remove");
        util.Component.add(this,
                this.removeButton,
                GridBagConstraints.HORIZONTAL,
                1, 1, 1, 1, 0.5f, 0.5f
        );

        this.returnButton = new JButton("Return");
        util.Component.add(this,
                this.returnButton,
                GridBagConstraints.HORIZONTAL,
                0, 2, 1, 1, 0.5f, 0.5f
        );

        this.addCollectionButton = new JButton("Add to Collection");
        util.Component.add(this,
                this.addCollectionButton,
                GridBagConstraints.HORIZONTAL,
                1, 2, 1, 1, 0.5f, 0.5f
        );

        this.sortSectionLabel = new JLabel("Sort Section");
        util.Component.add(this,
                this.sortSectionLabel,
                GridBagConstraints.HORIZONTAL,
                0, 3, 1, 1, 0.5f, 0.5f
        );

        this.sortSectionText = new JTextField();
        util.Component.add(this,
                this.sortSectionText,
                GridBagConstraints.HORIZONTAL,
                1, 3, 1, 1, 0.5f, 0.5f
        );

        this.randColSortButton = new JButton("Random Collection Sort");
        util.Component.add(this,
                this.randColSortButton,
                GridBagConstraints.HORIZONTAL,
                1, 4, 1, 1, 0.0f, 0.0f
        );

        this.mostlySortedSortButton = new JButton("Mostly Sorted Sort");
        util.Component.add(this,
                this.mostlySortedSortButton,
                GridBagConstraints.HORIZONTAL,
                1, 5, 1, 1, 0.0f, 0.0f
        );

        this.reverseOrderSortButton = new JButton("Reverse Order Sort");
        util.Component.add(this,
                this.reverseOrderSortButton,
                GridBagConstraints.HORIZONTAL,
                1, 6, 1, 1, 0.0f, 0.0f
        );

        this.exitButton = new JButton("Exit");
        util.Component.add(this,
                this.exitButton,
                GridBagConstraints.HORIZONTAL,
                0, 7, 1, 1, 0, 0
        );
    }

    private void createActionListeners()
    {
        this.retrieveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CDRecord record = archiveListPanel.getSelectedCDRecord();
                if (record == null)
                    return;

                processLogPanel.logProcess("SENT - " + "Retrieve Item" + " - " + record.getBarcode());
                sendRecord(record, "retrieve");
            }
        });

        this.removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CDRecord record = archiveListPanel.getSelectedCDRecord();
                if (record == null)
                    return;

                processLogPanel.logProcess("SENT - " + "Remove Item" + " - " + record.getBarcode());
                sendRecord(record, "remove");
            }
        });

        this.returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CDRecord record = archiveListPanel.getSelectedCDRecord();
                if (record == null)
                    return;

                processLogPanel.logProcess("SENT - " + "Return Item" + " - " + record.getBarcode());
                sendRecord(record, "return");
            }
        });

        this.addCollectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CDRecord record = archiveListPanel.getSelectedCDRecord();
                if (record == null)
                    return;

                processLogPanel.logProcess("SENT - " + "Add Item to Collection" + " - " + record.getBarcode());
                sendRecord(record, "add");
            }
        });

        this.randColSortButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CDRecord record = archiveListPanel.getSelectedCDRecord();
                if (record == null)
                    return;

                processLogPanel.logProcess("SENT - " + "" + " - " + record.getBarcode());
                sendRecord(record, "sort");
            }
        });

        this.mostlySortedSortButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CDRecord record = archiveListPanel.getSelectedCDRecord();
                if (record == null)
                    return;

                processLogPanel.logProcess("SENT - " + "Add Item to Collection" + " - " + record.getBarcode());
                sendRecord(record, "sort");
            }
        });

        this.reverseOrderSortButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CDRecord record = archiveListPanel.getSelectedCDRecord();
                if (record == null)
                    return;

                processLogPanel.logProcess("SENT - " + "Add Item to Collection" + " - " + record.getBarcode());
                sendRecord(record, "sort");
            }
        });

        this.exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                records.saveCSV("records.csv");
                records.saveList("records.data");
                System.exit(0);
            }
        });
    }

    private void sendRecord(CDRecord record, String action)
    {
        this.client.send(action + ";" + (sortSectionText.getText().isEmpty() ? "null" : sortSectionText.getText()) + ";" + record.getTitle() + ";" + record.getAuthor() + ";" + record.getSection() + ";" + record.getX() + ";" + record.getY() + ";" + record.getBarcode() + ";" + record.getDescription() + ";" + (record.isOnLoan() ? "Yes" : "No"));
    }
}

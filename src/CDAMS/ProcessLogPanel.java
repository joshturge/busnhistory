package CDAMS;

import lists.DoublyLinkedList;
import trees.BinaryTree;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessLogPanel extends JPanel {
    private JLabel processLogLabel, hashmapSetLabel, displayBinaryTreeLabel;
    private JButton processLogButton, saveButton, displayHashmapButton, preOrderButton, inOrderButton, postOrderButton;
    private JTextArea processLogTextArea;

    private DoublyLinkedList processLog;
    private BinaryTree recordTree;
    private HashMap<String, String> recordMap = new HashMap<>();

    public ProcessLogPanel()
    {
        this.processLog = new DoublyLinkedList();
        createUI();
        createActionListeners();
    }

    private void createUI()
    {
        this.setLayout(new GridBagLayout());

        this.processLogLabel = new JLabel("Process Log:");
        util.Component.add(this,
                this.processLogLabel,
                GridBagConstraints.BOTH,
                0, 0, 1, 1, 40.0f, 0.0f
        );

        this.processLogButton = new JButton("Process Log");
        util.Component.add(this,
                this.processLogButton,
                GridBagConstraints.HORIZONTAL,
                1, 0, 4, 1, 0.0f, 0.0f
        );

        this.processLogTextArea = new JTextArea();
        JScrollPane sp = new JScrollPane(this.processLogTextArea);
        sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        util.Component.add(this,
                sp,
                GridBagConstraints.BOTH,
                0, 1, 4, 1, 50.0f, 0.5f
        );

        this.displayBinaryTreeLabel = new JLabel("Display Binary Tree:");
        util.Component.add(this,
                this.displayBinaryTreeLabel,
                GridBagConstraints.HORIZONTAL,
                0, 2, 1, 1, 0, 0
        );

        this.preOrderButton = new JButton("Pre-Order");
        util.Component.add(this,
                this.preOrderButton,
                GridBagConstraints.HORIZONTAL,
                1, 2, 1, 1, 0, 0
        );

        this.inOrderButton = new JButton("In-Order");
        util.Component.add(this,
                this.inOrderButton,
                GridBagConstraints.HORIZONTAL,
                2, 2, 1, 1, 0, 0
        );

        this.postOrderButton = new JButton("Post-Order");
        util.Component.add(this,
                this.postOrderButton,
                GridBagConstraints.HORIZONTAL,
                3, 2, 1, 1, 0, 0
        );

        this.hashmapSetLabel = new JLabel("Hashmap / Set:");
        util.Component.add(this,
                this.hashmapSetLabel,
                GridBagConstraints.HORIZONTAL,
                0, 3, 1, 1, 0, 0
        );

        this.saveButton = new JButton("Save");
        util.Component.add(this,
                this.saveButton,
                GridBagConstraints.HORIZONTAL,
                1, 3, 1, 1, 0, 0
        );

        this.displayHashmapButton = new JButton("Display");
        util.Component.add(this,
                this.displayHashmapButton,
                GridBagConstraints.HORIZONTAL,
                2, 3, 1, 1, 0, 0
        );
    }

    private void createActionListeners()
    {
        this.processLogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                processLogTextArea.setText("");
                processLogTextArea.setText(processLog.toString());
            }
        });

        this.preOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                processLogTextArea.setText("");
                processLogTextArea.setText("Binary Tree Data, Pre-Order:\n");
                displayTree(recordTree.traversePreOrder());
            }
        });

        this.inOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                processLogTextArea.setText("");
                processLogTextArea.setText("Binary Tree Data, In-Order:\n");
                displayTree(recordTree.traverseInOrder());
            }
        });

        this.postOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                processLogTextArea.setText("");
                processLogTextArea.setText("Binary Tree Data, Post-Order:\n");
                displayTree(recordTree.traversePostOrder());
            }
        });

        this.saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                for (BinaryTree.Node node : recordTree.traverseInOrder())
                    recordMap.put(Integer.toString(node.getKey()), node.getData().toString());
            }
        });

        this.displayHashmapButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                processLogTextArea.setText("");
                processLogTextArea.setText("HashMap of Binary Tree:\n");

                for(Map.Entry<String, String> entry : recordMap.entrySet())
                {
                    processLogTextArea.append(entry.getKey() + " = " + entry.getValue() + "\n");
                }
            }
        });
    }

    public void logProcess(String message)
    {
        this.processLog.append(message);
        this.processLogTextArea.append(message+"\n");
    }

    public void fillTree(List<CDRecord> records)
    {
        this.recordTree = new BinaryTree();

        for (CDRecord record : records)
        {
            this.recordTree.insert(new BinaryTree.Node(record.getBarcode(), record.getTitle()));
        }
    }

    private void displayTree(List<BinaryTree.Node> nodes)
    {
        for (BinaryTree.Node node : nodes)
        {
            this.processLogTextArea.append(node.getKey() + " - " + node.getData() + "\n");
        }
    }

}

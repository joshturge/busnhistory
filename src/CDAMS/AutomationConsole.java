package CDAMS;

import experiments.CDArchiveConsole;
import sockets.MessageListener;
import sockets.MessageSender;
import sockets.Server;
import sorting.Bubble;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AutomationConsole {
    private JFrame window;

    private JLabel currentRequestActionLabel, randomCollectionLabel,
            barcodeSelectedItemLabel, sectionLabel, titleLabel;

    private JComboBox actionComboBox;
    private String[] actions = {
            "Add",
            "Remove",
            "Retrieve",
            "Return",
            "Sort"
    };

    private JButton processButton, addItemButton;

    private JTextField barcodeSelectedItemText, sectionText;

    private JTable cdRecordsTable;
    private CDRecordTableModel tableData;

    private Server server;

    private List<CDRecord> records;

    public AutomationConsole()
    {
        this.window = new JFrame("Automation Console");
        this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.window.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent windowEvent) {

            }

            @Override
            public void windowClosing(WindowEvent windowEvent) {
            }

            @Override
            public void windowClosed(WindowEvent windowEvent) {
            }

            @Override
            public void windowIconified(WindowEvent windowEvent) {

            }

            @Override
            public void windowDeiconified(WindowEvent windowEvent) {

            }

            @Override
            public void windowActivated(WindowEvent windowEvent) {

            }

            @Override
            public void windowDeactivated(WindowEvent windowEvent) {

            }
        });
        this.window.getContentPane().setLayout(new GridBagLayout());

        createUI();
        addActionListeners();

        this.records = new ArrayList<>();

        this.tableData = new CDRecordTableModel(this.records);

        this.cdRecordsTable.setModel(this.tableData);
        this.cdRecordsTable.setFillsViewportHeight(true);

        (new Thread(new Runnable() {
            @Override
            public void run() {
                server = new Server(20000, new MessageListener() {
                    @Override
                    public void message(String msg, MessageSender sender) {
                        String[] dataColumns = msg.split(";");
                        CDRecord record = new CDRecord(
                                dataColumns[2],
                                dataColumns[3],
                                dataColumns[4],
                                Integer.parseInt(dataColumns[5]),
                                Integer.parseInt(dataColumns[6]),
                                Integer.parseInt(dataColumns[7]),
                                dataColumns[8],
                                dataColumns[9] == "Yes"
                        );

                        if (!dataColumns[1].contains("null"))
                            sectionText.setText(dataColumns[1]);

                        records.add(record);
                        Bubble.sort(records, Bubble.SortBy.SECTION);
                        tableData.fireTableDataChanged();

                        // WAIT HERE
                        try
                        {
                            Thread.sleep(3000);
                        } catch(Exception e)
                        {
                            System.err.println("Thread cannot sleep" + e.getMessage());
                        }

                        if (dataColumns[0].contains("retrieve"))
                        {
                            actionComboBox.setSelectedIndex(2);
                            sender.send("RCVD - Item Retrieved - " + record.getBarcode());
                        }
                        else if (dataColumns[0].contains("remove"))
                        {
                            actionComboBox.setSelectedIndex(1);
                            sender.send("RCVD - Item Removed - " + record.getBarcode());
                        }
                        else if (dataColumns[0].contains("return"))
                        {
                            actionComboBox.setSelectedIndex(3);
                            sender.send("RCVD - Item Returned - " + record.getBarcode());
                        }
                        else if (dataColumns[0].contains("add"))
                        {
                            actionComboBox.setSelectedIndex(0);
                            sender.send("RCVD - Item Added to Collection - " + record.getBarcode());
                        }
                        else if (dataColumns[0].contains("sort"))
                        {
                            actionComboBox.setSelectedIndex(4);
                            sender.send("RCVD - Sort Items - " + record.getBarcode());
                        }
                    }
                });
            }
        })).start();

        this.window.pack();
        this.window.setMinimumSize(new Dimension(700, 400));
        this.window.setSize(new Dimension(700, 400));
        this.window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new AutomationConsole();
            }
        });

    }

    public void createUI()
    {
        this.currentRequestActionLabel = new JLabel("Current Requested Action:");
        util.Component.add(this.window.getContentPane(),
                this.currentRequestActionLabel,
                GridBagConstraints.BOTH,
                0, 0, 1, 1, 0, 0
        );

        this.actionComboBox = new JComboBox(this.actions);
        util.Component.add(this.window.getContentPane(),
                this.actionComboBox,
                GridBagConstraints.BOTH,
                1, 0, 1, 1, 0, 0
        );

        this.processButton = new JButton("Process");
        util.Component.add(this.window.getContentPane(),
                this.processButton,
                GridBagConstraints.BOTH,
                2, 0, 1, 1, 0, 0
        );

        this.barcodeSelectedItemLabel = new JLabel("Barcode of selected Item:");
        util.Component.add(this.window.getContentPane(),
                this.barcodeSelectedItemLabel,
                GridBagConstraints.BOTH,
                0, 1, 1, 1, 0, 0
        );

        this.barcodeSelectedItemText = new JTextField();
        util.Component.add(this.window.getContentPane(),
                this.barcodeSelectedItemText,
                GridBagConstraints.BOTH,
                1, 1, 1, 1, 3, 0
        );

        this.sectionLabel = new JLabel("Section:");
        util.Component.add(this.window.getContentPane(),
                this.sectionLabel,
                GridBagConstraints.BOTH,
                2, 1, 1, 1, 0, 0
        );

        this.sectionText = new JTextField();
        util.Component.add(this.window.getContentPane(),
                this.sectionText,
                GridBagConstraints.BOTH,
                3, 1, 1, 1, 1, 0
        );

        this.addItemButton = new JButton("Add Item");
        util.Component.add(this.window.getContentPane(),
                this.addItemButton,
                GridBagConstraints.BOTH,
                4, 1, 1, 1, 0, 0
        );

        this.titleLabel = new JLabel("Archive CDs");
        util.Component.add(this.window.getContentPane(),
                this.titleLabel,
                GridBagConstraints.CENTER,
                1, 2, 1, 1, 0, 0
        );

        this.cdRecordsTable = new JTable();
        util.Component.add(this.window.getContentPane(),
                this.cdRecordsTable,
                GridBagConstraints.BOTH,
                1, 3, 1, 1, 0,0
        );
    }

    private void addActionListeners()
    {
        this.cdRecordsTable.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                int selectedCDRecordIndex = cdRecordsTable.getSelectedRow();
                barcodeSelectedItemText.setText(Integer.toString(records.get(selectedCDRecordIndex).getBarcode()));
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });

        this.processButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
            }
        });
    }
}

package experiments;

import CDAMS.*;
import sorting.Bubble;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class CDArchiveConsole {
   JFrame window;

   CDRecordStorage records;
   CDRecordPanel cdRecordPanel;
   ArchiveListPanel archiveListPanel;
   ActionRequestPanel actionRequestPanel;
   ProcessLogPanel processLogPanel;

   public CDArchiveConsole()
   {
      this.records = new CDRecordStorage("Sample_CD_Archive_Data.txt");
      this.cdRecordPanel = new CDRecordPanel(this.records);
      this.processLogPanel = new ProcessLogPanel();
      this.archiveListPanel = new ArchiveListPanel(this.records, this.cdRecordPanel, this.processLogPanel);
      this.actionRequestPanel = new ActionRequestPanel(this.records, this.archiveListPanel, this.processLogPanel);

      this.window = new JFrame("Archive Management Console");
      this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      this.window.addWindowListener(new WindowListener() {
         @Override
         public void windowOpened(WindowEvent windowEvent) {

         }

         @Override
         public void windowClosing(WindowEvent windowEvent) {
         }

         @Override
         public void windowClosed(WindowEvent windowEvent) {

         }

         @Override
         public void windowIconified(WindowEvent windowEvent) {

         }

         @Override
         public void windowDeiconified(WindowEvent windowEvent) {

         }

         @Override
         public void windowActivated(WindowEvent windowEvent) {

         }

         @Override
         public void windowDeactivated(WindowEvent windowEvent) {

         }
      });
      this.window.getContentPane().setLayout(new GridBagLayout());

      createUI();

      this.window.pack();
      this.window.setMinimumSize(new Dimension(700, 400));
      this.window.setSize(new Dimension(700, 400));
      this.window.setVisible(true);
   }

   private void createUI()
   {
      addComponent(window.getContentPane(),
              this.archiveListPanel,
              GridBagConstraints.BOTH,
              0, 1, 3, 1,
              70, 40);

      addComponent(window.getContentPane(),
              this.processLogPanel,
              GridBagConstraints.BOTH,
              0, 2, 3, 1,
              70, 40);

      addComponent(window.getContentPane(),
              this.cdRecordPanel,
              GridBagConstraints.BOTH,
              3, 0, 1, 2,
              30, 40);

      addComponent(window.getContentPane(),
              this.actionRequestPanel,
              GridBagConstraints.BOTH,
              3, 2, 1, 1,
              30, 40);
   }

   private <C extends Component> void addComponent(
           Container contentPane,
           C component,
           int fill, int gridX, int gridY,
           int gridWidth, int gridHeight,
           float weightX, float weightY
   )
   {
      GridBagConstraints constraints = new GridBagConstraints();
      constraints.fill = fill;
      constraints.gridx = gridX;
      constraints.gridy = gridY;
      constraints.gridwidth = gridWidth;
      constraints.gridheight = gridHeight;
      constraints.weightx = weightX;
      constraints.weighty = weightY;

      contentPane.add(component, constraints);
   }

   private <C extends Component> void addComponent(
           Container contentPane,
           C component,
           int fill, int gridX, int gridY,
           int gridWidth, int gridHeight,
           float weightX, float weightY,
           Insets insets, int anchor
   )
   {
      GridBagConstraints constraints = new GridBagConstraints();
      constraints.fill = fill;
      constraints.gridx = gridX;
      constraints.gridy = gridY;
      constraints.gridwidth = gridWidth;
      constraints.gridheight = gridHeight;
      constraints.weightx = weightX;
      constraints.weighty = weightY;
      constraints.insets = insets;
      constraints.anchor = anchor;

      contentPane.add(component, constraints);
   }

   public static void main(String[] args) {
      SwingUtilities.invokeLater(new Runnable() {
         @Override
         public void run() {
            new CDArchiveConsole();
         }
      });
   }
}

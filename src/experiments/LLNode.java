package experiments;

public class LLNode {
   public static void main(String[] args) {
      LLNode myNode = new LLNode("Hello");

      myNode.append(new LLNode("this"));
      myNode.append(new LLNode("is"));
      myNode.append(new LLNode("a"));
      myNode.append(new LLNode("list"));

      System.out.println(myNode.toString());

      if (myNode.find("list") != null)
         System.out.println("Found Node");
      else
         System.out.println("Not Found");


   }

   private Object data;
   private LLNode next;

   public LLNode(Object data)
   {
      this.data = data;
   }

   public String toString()
   {
      String str = "Linked List: ";
       LLNode node = this;

       while (node.next != null)
       {
          str += node.data.toString();
          if (node.next != null)
             str += " -> ";
          node = node.next;
       }

       return str;
   }

   public void append(LLNode new_node)
   {
      LLNode last = this;

      while (last.next != null)
          last = last.next;

      last.next = new_node;
   }

   public LLNode find(Object data)
   {
      LLNode node = this;

      while (node != null)
      {
         if (node.data == data)
            return node;
         else
            node = node.next;
      }

      return null;
   }
}

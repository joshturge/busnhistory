package experiments;

import java.util.HashMap;
import java.util.Scanner;

public class HelloHashMap {
    public static void main(String[] args) {
        int[] numbers = new int[] {10, 45, 24, 46, 76};

        System.out.println(numbers[2]);

        HashMap<String, String> fruitToColour = new HashMap<>();
        fruitToColour.put("banana", "yellow");
        fruitToColour.put("apple", "green");
        fruitToColour.put("apple", "red");
        fruitToColour.put("pumpkin", "orange");
        fruitToColour.put("tomato", "red");

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine())
        {
            String fruit = scanner.nextLine();

            if (fruit.equalsIgnoreCase("q"))
                break;
            String colour = fruitToColour.get(fruit);

            if (!colour.isEmpty())
                System.out.println(colour);
            else
                System.out.println("Colour not found");
        }



    }
}
